FROM mono

MAINTAINER wearingstorm

# Allow use of editors like nano/vi/vim
ENV TERM xterm

RUN apt-get update && \

    # install nano and other requirements
    apt-get -qq install nano tzdata && \

    # minimize image by removing unnecessary stuff
    apt-get autoremove -qq && apt-get clean all && rm -rf /usr/share/doc /usr/share/man /var/log/* /tmp/*


# Set the timezone
RUN echo Europe/Oslo | tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

# add folder needed
COPY dmp-server /app/dmp-server

# change permissions and move resource directory to a safe place
COPY run.sh /app/dmp-server/run.sh
COPY update.sh /app/dmp-server/update.sh

# Switch current working directory
WORKDIR /app/dmp-server

# expose dmp port
EXPOSE 6702

# Entrypoint that updates DMP
COPY entry.sh /app/entry.sh

ENTRYPOINT ["/app/entry.sh"]
CMD ["/app/dmp-server/run.sh"]

# Make info file about this build
RUN printf "Build of wearingstorm/dmp:latest, date: %s\n"  `date -u +"%Y-%m-%dT%H:%M:%SZ"`
